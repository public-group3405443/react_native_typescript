import { Text, View } from 'react-native'
import React, { Component } from 'react'

interface MyProps{
    name:String,
    email:String,
    age:Number,
}

interface MyState{
    counter: Number,
}

export default class MyClassCompWithTsx extends Component<MyProps,MyState> {
  render() {
    return (
      <View>
        <Text>MyClassCompWithTsx</Text>
      </View>
    )
  }
}