const mongoose = require("mongoose")
const Notes = new mongoose.Schema({
    title: {
        type: String,
        min: 5,
        max: 50,
        unique: true,
        require: true
    },
    description: {
        type: String,
        min: 30,
        max: 200,
        require: true
    },

},{ timestamps: true });
module.exports = mongoose.model("Notes",Notes)