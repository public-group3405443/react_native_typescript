const router = require("express").Router()
const User = require("../models/User");


router.post ("/login",async (req, res) => {
    res.send("user logged in ") 
    try {
        const user = await User.findOne({email:req.body.email})
        !user && res.status(404).json({"message":"user is not found"})
        const validatePassword = req.body.password == user.password
        !validatePassword && res.status(404).json({"message":"password is wrong"})
        res.status(200).json({user:user,"message":"user found"})
    } catch (error) {
        res.status(500).json(error)
    }
})
router.post("/register", async (req, res) => {
    // res.send("user registered ")
    // const user = new User({
    //     name :req.body.name,
    //     email :req.body.email,
    //     password :req.body.password
    // })
    // const data = await user.save()
    // res.status(200).json(data)
    try {
        const user = new User({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        })
        const data = await user.save()
        res.status(200).json(data)
    } catch (error) {
        console.log("Viraj",error)
        res.status(500).json(error);
    }
})

module.exports = router