const express = require("express")
const dotenv = require("dotenv")
const mongoose = require("mongoose")
const morgan = require("morgan")
const helmet = require("helmet")
const authRouter = require("./routes/auth");
const app = express();

dotenv.config()

app.use(express.json())
app.use(morgan("common"))
app.use(helmet())




// mongodb connection
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser:true,useUnifiedTopology:true}).then(()=>{
    console.log("Hey Database connected");
}).catch(error=>{
    console.log("error in connecting database",error)
})


app.get("/notes",(req,res)=>{
    res.send("notes fetched success fully 12")
})

app.use("/api/auth",authRouter)


app.listen(9393,()=>{
    console.log("Server started at port no",app.listen().address().port)
    
})