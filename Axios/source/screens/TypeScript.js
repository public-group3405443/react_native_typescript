import { View, Text } from 'react-native'
import React from 'react'
import MyClassCompWithTsx from './MyClassCompWithTsx'

const TypeScript = () => {
  return (
    <View>
      <Text>TypeScript</Text>
      <MyClassCompWithTsx/>
    </View>
  )
}

export default TypeScript