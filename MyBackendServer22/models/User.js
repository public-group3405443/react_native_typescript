const mongoose = require("mongoose")
const User = new mongoose.Schema({
    name: {
        type: String,
        min: 5,
        max: 30,
        unique: true,
        require: true
    },
    email: {
        type: String,
        min: 5,
        max: 30,
        unique: true,
        require: true
    },
    password: {
        type: String,
        min: 5,
        max: 30,
        require: true
    }
},{timestamps:true});
module.exports = mongoose.model("user", User)